//
//  Constants.swift
//  pixel-city
//
//  Created by Chirag Tailor on 03/09/2017.
//  Copyright © 2017 Chirag Tailor. All rights reserved.
//

import Foundation


let apiKey = "23c776f1faaa519f2011f09fcb610e14"

func flickrUrl(forApi key: String, withAnnotation annotation: DroppablePin, andNumberOfPhotos number: Int) -> String {
    return "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&lat=\(annotation.coordinate.latitude)&lon=\(annotation.coordinate.longitude)&radius=1&radius_units=mi&per_page=\(number)&format=json&nojsoncallback=1"
}
