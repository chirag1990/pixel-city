//
//  PhotoCell.swift
//  pixel-city
//
//  Created by Chirag Tailor on 03/09/2017.
//  Copyright © 2017 Chirag Tailor. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
